//
//  RequestHandler.swift
//  Final_Week_Asignement
//
//  Created by Rastislav Smolen on 10/11/2019.
//  Copyright © 2019 Rastislav Smolen. All rights reserved.
//

import Foundation
import UIKit.UIImage

enum RequestPath: String {
    case swift
}
enum Outcome<T> {
       case success(T)
       case failure(String)
   }

class RequestHandler
{
    
    let imageCache: NSCache<NSString, UIImage>
       
       init() { imageCache = NSCache<NSString, UIImage>() }

    func fetch(_ requestPath:RequestPath ,completion: @escaping (Data?) -> Void) {
      //  let queryToken = URLQueryItem(name: curl ,value: #"\"#)
       // let searchTopicQueryToken = URLQueryItem(name: "search", value: nil)
        let searchItemQueryToken = URLQueryItem(name: "q", value: requestPath.rawValue)
        //let orderItemQueryToken = URLQueryItem(name: "sort", value: "stars")
        
            var urlComponents = URLComponents()
          //  urlComponents.queryItems = [queryToken]
            urlComponents.scheme = "https"
            urlComponents.host = "api.github.com"
            urlComponents.path = "/search/repositories"
            urlComponents.queryItems = [searchItemQueryToken]   
     
        guard let url = urlComponents.url else { return }
                
                let dataTask = URLSession.shared.dataTask(with: url) { (data, response, error) in
                    
                    if let error = error {
                        print(error.localizedDescription)
                        completion(nil)
                    } else if let data = data, let response = response as? HTTPURLResponse {
                        
                        switch response.statusCode {
                        case 200...299:
                            completion(data)
                        default:
                            print("Invalid HTTP status code \(response.statusCode)")
                            completion(nil)
                        }
                        
                    }
                }
                
                dataTask.resume()
                
            }
            
        }
    protocol Parser {
        func parse<T>(_ data: Data, into: T.Type, completion: (Outcome<T>) -> Void) where T: Codable
    }

    struct JSONParser: Parser {
        
        func parse<T>(_ data: Data, into: T.Type, completion: (Outcome<T>) -> Void) where T : Decodable, T : Encodable {
            
            do {
                let result = try JSONDecoder().decode(into, from: data)
                completion(.success(result))
                
            } catch {
              debugPrint(error)
            }
            
        }
        
}
