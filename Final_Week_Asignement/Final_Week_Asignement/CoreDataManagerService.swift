//
//  CoreDataManagerService.swift
//  Final_Week_Asignement
//
//  Created by Rastislav Smolen on 11/11/2019.
//  Copyright © 2019 Rastislav Smolen. All rights reserved.
//

import Foundation
import CoreData

// MARK: - CD Stack
class CoreDataManagerService {
    
    private init() {}
    
    static let shared = CoreDataManagerService()
    
    
    var mainContext: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    var privateContext: NSManagedObjectContext {
        let newConetxt = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        newConetxt.parent = mainContext
        return newConetxt
    }
    
    lazy var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "Final_Week_Asignement")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

//    @discardableResult
    func saveContext() -> Bool {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
                return true
            } catch {
                
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
        return false
    }
    
}
