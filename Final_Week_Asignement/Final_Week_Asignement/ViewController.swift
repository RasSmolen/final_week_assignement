//
//  ViewController.swift
//  Final_Week_Asignement
//
//  Created by Rastislav Smolen on 10/11/2019.
//  Copyright © 2019 Rastislav Smolen. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController
{
    var  model : NetworkingModel?
    var dataModel : DataModel?
    
    let coreDataManagerService = CoreDataManagerService.shared
    
    var dataManager = DataManager()
   
    let requestHandler = RequestHandler()
    
    private var repoContent:[NSManagedObject] = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let content = CommitContent(context: coreDataManagerService.mainContext)
//saveToCoreData(named: htmlUrlAccount, withUrl: avatarUrl)
        model = NetworkingModel()
        dataModel = DataModel(coreDataManagerService)
        fetchUsers()
        
        
    }
    
    
    func fetchUsers() {
        
        // Use the requestHandler to fetch the Users
        requestHandler.fetch(.swift) { (jsonData) in
            
            guard let jsonData = jsonData else { return }
            
            JSONParser().parse(jsonData, into: Commits.self, completion: { [unowned self] outcome in
                
                switch outcome {
                case .failure(let error): print(error)
                case .success(let data): self.dataManager.commits = data
                    
                }
                print(self.dataManager.commits!)
    
                
            })
            
        }
        
    }
   
}
