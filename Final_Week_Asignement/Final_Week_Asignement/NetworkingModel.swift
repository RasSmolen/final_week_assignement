//
//  NetworkingModel.swift
//  Final_Week_Asignement
//
//  Created by Rastislav Smolen on 10/11/2019.
//  Copyright © 2019 Rastislav Smolen. All rights reserved.
import Foundation

// MARK: - Commits
struct Commits: Codable {
    let totalCount: Int
    let incompleteResults: Bool
    let items: [Item]

    enum CodingKeys: String, CodingKey {
        case totalCount = "total_count"
        case incompleteResults = "incomplete_results"
        case items
    }
}

// MARK: - Item
struct Item: Codable {
    let id: Int?
    let name : String?
    let fullName: String?
    let itemDescription: String?
    let homepage: String?
    let watchersCount: Int?
    let language: String?
    let forksCount: Int?
    let openIssuesCount: Int?
    let forks : Int?
    let openIssues: Int?
    let watchers: Int?
    let score: Double?
    let owner: Owner
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case fullName = "full_name"
        case itemDescription = "description"
        case homepage
        case watchersCount = "watchers_count"
        case language
        case forksCount = "forks_count"
        case openIssuesCount = "open_issues_count"
        case forks
        case openIssues = "open_issues"
        case watchers
        case score
        case owner = "owner"
    }
    
}

struct Owner: Codable {
    
    let avatarUrl: String?
    let htmlUrlAccount: String?
    
    private enum CodingKeys: String, CodingKey {
        case avatarUrl = "avatar_url"
        case htmlUrlAccount = "html_url"
    }
}
struct DataManager
{
    var name : [Item]?
    var commits : Commits?
    
}
class NetworkingModel
{
   
    init()
    {
       
       
    }
    
}

// class CoreDataManager {
//
//     let data: Commits
//     let modelAccount = AccountModel(CoreDataStack.shared)
//     let modelCodeRepository = CodeRepositoryModel(coreDataStack: CoreDataStack.shared)
//
//     init(data: RootNode) {
//
//         self.data = data
//     }
//
//     internal func saveData() {
//
//         for (index, element) in data.repositories.enumerated() {
//
//             modelAccount.updateAccount(named: element.repoOwner.htmlUrlAccount, withAvatarUrl: element.repoOwner.avatarUrl)
//         }
//
//         for (index, element) in data.repositories.enumerated() {
//
//             let accountUpdateCreate = modelAccount.fetchAccount(named: element.repoOwner.htmlUrlAccount)
//             modelCodeRepository.setCurrentAccount(curentAccountToSet: accountUpdateCreate[0] as! Account)
//             modelCodeRepository.updateCodeRepository(named: element.repoOwner.htmlUrlAccount, shortName: element.shortName, fullName: element.fullName, longDescription: element.longDescription, repoScore: element.repoScore, repoLanguage: element.repoLanguage, watchersCount: element.watchersCount, forksCount: element.forksCount, openIssuesCount: element.openIssuesCount, htmlUrlRepo: element.htmlUrlRepo)
//         }
//     }
// }
//
